"use strict";

var express = require( "express" );
var cors = require( "cors" );
var app = express();
var bodyParser = require( "body-parser" );
var Log = require( "log" );
var config = require( "./config.js" );

var log = new Log( "debug" );
var corsOptions = {
  origin: require( "./commons/origin" )( config.ORIGIN ),
  credentials: true
};

app.options( "*", cors( corsOptions ) );
app.use( cors( corsOptions ) );
log.debug( "Configuring server by JZV" );
app.use( bodyParser.urlencoded( {
  extended: true
} ) );
app.use( bodyParser.json() );

/***************** ROUTER **********************/
var middleware = require( "./login/middleware.js" );
var isAuthorized = middleware.isAuthorized;

var users = require( "./routes/user" );
app.use( "/api/users", isAuthorized, users );

var resources = require( "./routes/staffing_tool/resource" );
app.use( "/api/resources", isAuthorized, resources );

var projects = require( "./routes/staffing_tool/project" );
app.use( "/api/projects", isAuthorized, projects );

var authenticate = require( "./routes/auth" );
app.use( "/api/authenticate", authenticate );

app.get( "/", function ( req, res ) {
  return res.status( 200 ).end( "API is working correctly" );
} );

/**************** Global routes  **********************/

app.use( function ( req, res ) {
  res.status( 404 ).send( "Resource not found." );
} );

app.use( function ( err, req, res ) {
  console.error( err.stack );
  res.status( 500 ).send( "Server error" );
} );

/****************  Start server *************/

var port = config.PORT;
log.debug( "Loading connections to MongoDB" );
require( "./bd.js" ); //load mongo
app.listen( port );
log.info( "Server running on port", port );
module.exports = app;
