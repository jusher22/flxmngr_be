"use strict";

var db = "mongodb://dev:dev@ds111178.mlab.com:11178/devdb";


module.exports = {
  TOKEN_SECRET: process.env.TOKEN_SECRET || "I want carrot cake",
  MONGO_URI: process.env.MONGO_URI || db,
  PORT: process.env.PORT || 3001,
  ORIGIN: process.env.ORIGIN || "http://localhost:3000",
};
