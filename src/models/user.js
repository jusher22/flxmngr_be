"use strict";

var mongoose = require( "mongoose" );
var bcrypt = require( "bcrypt" );
var SALT_WORK_FACTOR = 10;

var user = {
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  email: {
    type: String,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  enabled: {
    type: Boolean,
    default: true
  },
  permissions: {
    list: {
      type: Boolean,
      default: false
    },
    read: {
      type: Boolean,
      default: false
    },
    create: {
      type: Boolean,
      default: false
    },
    edit: {
      type: Boolean,
      default: false
    },
    remove: {
      type: Boolean,
      default: false
    }
  }
};

var UserSchema = new mongoose.Schema( user );

//this code encrypts user's password using brcypt algorithm (derived from Blowfish)
UserSchema.pre( "save", function ( next ) {
  var user = this;

  //only if password has been modified or is new
  if ( !user.isModified( "password" ) ) {
    return next;
  }
  bcrypt.genSalt( SALT_WORK_FACTOR, function ( err, salt ) {
    if ( err ) {
      return next( err );
    }
    bcrypt.hash( user.password, salt, function ( err, hash ) {
      if ( err ) {
        return next( err );
      }
      //overrides user's password with hash
      user.password = hash;
      next();
    } );
  } );
} );

//this code implements password verification
UserSchema.methods.comparePassword = function ( passwordToCheck, cb ) {
  bcrypt.compare( passwordToCheck, this.password, function ( err, isMatch ) {
    if ( err ) {
      return cb( err );
    }
    cb( null, isMatch );
  } );
};

module.exports = mongoose.model( "User", UserSchema );
