"use strict";

var mongoose = require( "mongoose" );

var resource = {
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  email: {
    type: String,
    unique: true
  },
  capability: {
    type: String,
    required: true
  },
  enabled: {
    type: Boolean,
    default: true
  }
};

var ResourceSchema = new mongoose.Schema( resource );

module.exports = mongoose.model( "Resource", ResourceSchema );
