"use strict";

var mongoose = require( "mongoose" );

var project = {
  name: {
    type: String,
    required: true,
    unique: true
  },
  booking: [ {
    resource: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Resource"
    },
    dateStart: {
      type: Date,
      required: true
    },
    dateEnd: {
      type: Date,
      require: true
    },
    hours: {
      type: Number,
      default: 8
    }
  } ]
};

var ProjectSchema = new mongoose.Schema( project );

module.exports = mongoose.model( "Project", ProjectSchema );
