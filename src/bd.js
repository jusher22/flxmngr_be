"use strict";

var Log = require( "log" );
var log = new Log( "debug" );
var mongoose = require( "mongoose" );
mongoose.connect( require( "./config.js" ).MONGO_URI );

var db = mongoose.connection;

db.on( "error", function ( error ) {
  log.error( "connection error on error " + error );
} );
db.once( "open", function () {
  log.info( "connetion stablished" );
} );

module.exports = db;
