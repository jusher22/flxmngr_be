"use strict";

var User = require( "../models/user" );
var express = require( "express" );
var _ = require( "lodash" );
var router = express.Router();


/************************
    --- ROUTES---
************************/

//gets all enable users
router.get( "/", list );

//gets specific user
router.get( "/:id", user );

//gets users accordig to text query
router.get( "/search", search );

//inserts new user document
router.post( "/", save );

//updates existing user document
router.put( "/:id", update );

/************************
    --- Functions ---
************************/

//LIST
function list( req, res ) {
  User.find( {}, function ( err, docs ) {
    if ( err ) {
      return res.status( 500 ).send( err );
    }
    return res.json( docs );
  } );
}

//USER
function user( req, res ) {
  getUser( res )( req.params.id );
}

//SEARCH
function search( req, res ) {
  var query = {
    name: {
      "$regex": req.query.text,
      $options: "i"
    }
  };
  var options = {
    skip: req.query.page,
    limit: req.query.qty
  };
  User.find( query, null, options, function ( err, docs ) {
    if ( err ) {
      return res.status( 500 ).send( err );
    }
    return res.json( docs );
  } );
}

//UPDATE
function update( req, res ) {
  var id = req.params.id;
  var options = {
    multi: false,
    upsert: false
  };
  User.findOneAndUpdate( {
    "_id": id
  }, req.body, options, function ( err, docs ) {
    if ( err ) {
      return res.status( 500 ).send( err );
    }
    if ( _.isEmpty( docs ) ) {
      return res.status( 404 ).end();
    }
    return res.status( 200 ).json( docs );
  } );
}

//SAVE
function save( req, res ) {
  var newUser = new User( req.body );
  newUser.save( function ( err, doc ) {
    if ( err ) {
      return res.status( 500 ).send( err );
    }
    if ( _.isEmpty( doc ) ) {
      return res.status( 404 ).end();
    }
    return res.status( 201 ).json( doc._id );
  } );
}

/************************
    --- UTIL ---
************************/

function getUser( res ) {
  return function ( id ) {
    User.findOne( {
      "_id": id
    }, function ( err, doc ) {
      if ( err ) {
        return res.status( 500 ).send( err );
      }
      if ( _.isEmpty( doc ) ) {
        return res.status( 404 ).end();
      }
      return res.json( doc );
    } );
  };
}

module.exports = router;
