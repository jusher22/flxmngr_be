"use strict";

var User = require( "../models/user" );
var encode = require( "../login/middleware.js" ).createJWT;
var express = require( "express" );
var _ = require( "lodash" );
var router = express.Router();

/************************
    --- ROUTES---
************************/

router.post( "/login", authenticate );

/************************
    --- Functions ---
************************/

function authenticate( req, res ) {
  var query = {
    enabled: true,
    email: req.body.email
  };
  User.findOne( query, function ( err, user ) {
    //server connection error (or other server error)
    if ( err ) {
      return res.status( 500 ).send( err );
    }
    //username or email doesn not match in DB
    if ( _.isEmpty( user ) ) {
      return res.status( 404 ).end();
    }
    user.comparePassword( req.body.password, function ( err, isMatch ) {
      //server connection error (or other server error)
      if ( err ) {
        return res.status( 500 ).send( err );
      }
      // the password wont match
      if ( !isMatch ) {
        return res.status( 401 ).end();
      }
      return res.json( {
        user: user,
        token: encode( user )
      } );
    } );
  } );
}

module.exports = router;
