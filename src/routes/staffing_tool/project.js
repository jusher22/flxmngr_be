"use strict";

var Project = require( "../../models/staffing_tool/project" );
var express = require( "express" );
var _ = require( "lodash" );
var router = express.Router();


/************************
    --- ROUTES---
************************/

//gets all enabled projects
router.get( "/", list );

//gets specific project
router.get( "/:id", project );

//gets projects according to text query
router.get( "/search", search );

//inserts new project document
router.post( "/", save );

//updates existing project document
router.put( "/:id", update );

/************************
    --- Functions ---
************************/

//LIST
function list( req, res ) {
  Project.find( {} )
    .populate( "booking.resource" )
    .exec( function ( err, docs ) {
      if ( err ) {
        return res.status( 500 ).send( err );
      }
      return res.json( docs );
    } );
}

//Project
function project( req, res ) {
  getProject( res )( req.params.id );
}

//SEARCH
function search( req, res ) {
  var query = {
    name: {
      "$regex": req.query.text,
      $options: "i"
    }
  };
  var options = {
    skip: req.query.page,
    limit: req.query.qty
  };
  Project.find( query, null, options, function ( err, docs ) {
    if ( err ) {
      return res.status( 500 ).send( err );
    }
    return res.json( docs );
  } );
}

//UPDATE
function update( req, res ) {
  var id = req.params.id;
  var options = {
    multi: false,
    upsert: false
  };
  Project.findOneAndUpdate( {
    "_id": id
  }, req.body, options, function ( err, docs ) {
    if ( err ) {
      return res.status( 500 ).send( err );
    }
    if ( _.isEmpty( docs ) ) {
      return res.status( 404 ).end();
    }
    return res.status( 200 ).json( docs );
  } );
}

//SAVE
function save( req, res ) {
  var newProject = new Project( req.body );
  newProject.save( function ( err, doc ) {
    if ( err ) {
      return res.status( 500 ).send( err );
    }
    if ( _.isEmpty( doc ) ) {
      return res.status( 404 ).end();
    }
    return res.status( 201 ).json( doc._id );
  } );
}

/************************
    --- UTIL ---
************************/

function getProject( res ) {
  return function ( id ) {
    Project.findOne( {
      "_id": id
    }, function ( err, doc ) {
      if ( err ) {
        return res.status( 500 ).send( err );
      }
      if ( _.isEmpty( doc ) ) {
        return res.status( 404 ).end();
      }
      return res.json( doc );
    } );
  };
}

module.exports = router;
