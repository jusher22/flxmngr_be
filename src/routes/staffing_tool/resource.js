"use strict";

var Resource = require( "../../models/staffing_tool/resource" );
var express = require( "express" );
var _ = require( "lodash" );
var router = express.Router();


/************************
    --- ROUTES---
************************/

//gets all resources
router.get( "/", list );

//gets all enabled resources
router.get( "/enabled", enabled );

//gets specific resource
router.get( "/:id", resource );

//gets resources according to text query
router.get( "/search", search );

//inserts new resource document
router.post( "/", save );

//updates existing resource document
router.put( "/:id", update );

/************************
    --- Functions ---
************************/

//LIST
function list( req, res ) {
  Resource.find( {}, function ( err, docs ) {
    if ( err ) {
      return res.status( 500 ).send( err );
    }
    return res.json( docs );
  } );
}

//enabled
function enabled( req, res ) {
  Resource.find( {
    "enabled": true
  }, function ( err, docs ) {
    if ( err ) {
      return res.status( 500 ).send( err );
    }
    return res.json( docs );
  } );
}

//RESOURCE
function resource( req, res ) {
  getResource( res )( req.params.id );
}

//SEARCH
function search( req, res ) {
  var query = {
    name: {
      "$regex": req.query.text,
      $options: "i"
    }
  };
  var options = {
    skip: req.query.page,
    limit: req.query.qty
  };
  Resource.find( query, null, options, function ( err, docs ) {
    if ( err ) {
      return res.status( 500 ).send( err );
    }
    return res.json( docs );
  } );
}

//UPDATE
function update( req, res ) {
  var id = req.params.id;
  var options = {
    multi: false,
    upsert: false
  };
  Resource.findOneAndUpdate( {
    "_id": id
  }, req.body, options, function ( err, docs ) {
    if ( err ) {
      return res.status( 500 ).send( err );
    }
    if ( _.isEmpty( docs ) ) {
      return res.status( 404 ).end();
    }
    return res.status( 200 ).json( docs );
  } );
}

//SAVE
function save( req, res ) {
  var newResource = new Resource( req.body );
  newResource.save( function ( err, doc ) {
    if ( err ) {
      return res.status( 500 ).send( err );
    }
    if ( _.isEmpty( doc ) ) {
      return res.status( 404 ).end();
    }
    return res.status( 201 ).json( doc._id );
  } );
}

/************************
    --- UTIL ---
************************/

function getResource( res ) {
  return function ( id ) {
    Resource.findOne( {
      "_id": id
    }, function ( err, doc ) {
      if ( err ) {
        return res.status( 500 ).send( err );
      }
      if ( _.isEmpty( doc ) ) {
        return res.status( 404 ).end();
      }
      return res.json( doc );
    } );
  };
}

module.exports = router;
