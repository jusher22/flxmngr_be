"use strict";

var nodemon = require( "nodemon" );
module.exports = server;

function server() {
  return nodemon( {
    script: "src/index.js"
  } );
}
