"use strict";

var gulp = require( "gulp" );
var size = require( "gulp-size" )
var notify = require( "gulp-notify" );
var _ = require( "lodash" );
var routes = require( "../routes" );

module.exports = scripts;

function scripts() {
  var all = [ routes.scripts.main ];
  console.log( all, "before union" );
  all = _.union( routes.scripts.watch, [] );
  console.log( all, "after union" );
  var s = size( {
    showFiles: true
  } );
  return gulp.src( all )
    .pipe( s )
    .pipe( notify( {
      onLast: true,
      message: function () {
        return "Total file size: " + s.prettySize;
      }
    } ) );
}
