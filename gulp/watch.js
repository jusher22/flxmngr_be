"use strict";

var gulp = require( "gulp" );
var routes = require( "./routes" );

module.exports = watch;

function watch() {
  gulp.watch( routes.scripts.watch, [] );
}
