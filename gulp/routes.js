"use strict";

var main = "./src/index.js";
var watch = [ main, "src/*.js", "src/**/*.js" ];
var test = [ "test/*.js", "test/**/*.js" ];

module.exports = {
  scripts: {
    main: main,
    watch: watch,
    test: test,
    all: watch.concat( test )
  }
};
