"use strict";

var gulp = require( "gulp" );

require( "./other/tasks.js" );
require( "./qa/tasks.js" );


gulp.task( "serve", require( "./server" ) );
gulp.task( "default", [ "serve" ] );
